# run the signed-trusted script
Write-Host -ForegroundColor DarkYellow "Start signed-trusted scripts"
Write-Host -ForegroundColor DarkYellow "  Exec signed-trusted script"
. $PSScriptRoot\signed-trusted\test.ps1

# run the unsigned-trusted script
Write-Host ""
Write-Host -ForegroundColor DarkYellow "Start unsigned-trusted scripts"
Write-Host -ForegroundColor DarkYellow "  Exec unsigned-trusted script"
. $PSScriptRoot\unsigned-trusted\test.ps1

# run the signed-untrusted script
Write-Host ""
Write-Host -ForegroundColor DarkYellow "Start signed-untrusted scripts"
Write-Host -ForegroundColor DarkYellow "  Exec signed-untrusted script"
. $PSScriptRoot\signed-untrusted\test.ps1

# run the unsigned-untrusted script
Write-Host ""
Write-Host -ForegroundColor DarkYellow "Start unsigned-untrusted scripts"
Write-Host -ForegroundColor DarkYellow "  Exec unsigned-untrusted script"
. $PSScriptRoot\unsigned-untrusted\test.ps1