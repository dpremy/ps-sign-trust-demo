# create empty directories
New-Item -Path $PSScriptRoot\ -Name signed-untrusted -ItemType Directory -Force
New-Item -Path $PSScriptRoot\ -Name signed-trusted -ItemType Directory -Force
New-Item -Path $PSScriptRoot\ -Name unsigned-untrusted -ItemType Directory -Force
New-Item -Path $PSScriptRoot\ -Name unsigned-trusted -ItemType Directory -Force

# copy the source scripts to all the directories
Copy-Item -Force $PSScriptRoot\source\* $PSScriptRoot\signed-untrusted\
Copy-Item -Force $PSScriptRoot\source\* $PSScriptRoot\signed-trusted\
Copy-Item -Force $PSScriptRoot\source\* $PSScriptRoot\unsigned-untrusted\
Copy-Item -Force $PSScriptRoot\source\* $PSScriptRoot\unsigned-trusted\

# set test script to look like it's from another computer
Set-Content $PSScriptRoot\signed-untrusted\test.ps1 -Stream "Zone.Identifier" -Value "[ZoneTransfer]`nZoneId=3"
Set-Content $PSScriptRoot\unsigned-untrusted\test.ps1 -Stream "Zone.Identifier" -Value "[ZoneTransfer]`nZoneId=3"

# sign only the ps1 files in the signed test dirs
$cs_cert=Get-ChildItem Cert:\CurrentUser\My -CodeSigningCert | Where-Object {$_.HasPrivateKey -eq $TRUE} | Sort-Object -Property NotAfter | Select-Object -Last 1
Set-AuthenticodeSignature -FilePath $PSScriptRoot\signed-untrusted\*.ps1 -Certificate $cs_cert
Set-AuthenticodeSignature -FilePath $PSScriptRoot\signed-trusted\*.ps1 -Certificate $cs_cert