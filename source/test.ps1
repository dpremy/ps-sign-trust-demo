# run the script
Write-Host -ForegroundColor DarkYellow "    Using test.ps1 script"
. $PSScriptRoot\script.ps1

# run the module
Write-Host ""
Write-Host -ForegroundColor DarkYellow "    Exec module directly"
Import-Module $PSScriptRoot\module.psm1
Test-SigModule
Remove-Module module

Write-Host ""